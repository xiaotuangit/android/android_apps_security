Proxim应用程序

功能说明
    （1）将Contact类和Location类信息存储到本地文件中。

程序说明：
    （1）使用Context.getExternalFileDir()方法获取应用本地存储路径，
实际路径为外部存储根目录/Android/data/应用包名/对应目录下。
    （2）使用Environment.MEDIA_MOUNTED_READ_ONLY.equals(Environment.getExternalStorageState())
判断外部存储是否可读。
    （3）使用Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())
判断外部存储是否可写。
    （4）布局文件中限制EditText输入特定字符：
        android:inputType="phone"： 只能输入电话号码
        android:inputType="textEmailAddress"： 只能输入邮件地址字符
        android:inputType="numberDecimal"： 只能输入浮点数字字符
    （5）布局文件中限制EditText单行显示：
        android:singleLine="true"
    （6）布局文件中限制EditText弹出键盘时，Enter键的功能：
        android:imeOptions="actionNext"： 下一输入项
        android:imeOptions="actionDone"： 完成
    
