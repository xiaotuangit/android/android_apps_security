package com.qty.proxim;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class SaveController {

    private static final String TAG = "SaveController";

    public static boolean saveContact(Context context, Contact contact) {
        boolean result = false;
        if (isReadWrite()) {
            try {
                File outputFile = new File(context.getExternalFilesDir(null), contact.getFirstName());
                FileOutputStream outputStream = new FileOutputStream(outputFile);
                outputStream.write(contact.getBytes());
                outputStream.close();
                result = true;
            } catch (FileNotFoundException e) {
                Log.e(TAG, "File not found");
            } catch (IOException e) {
                Log.e(TAG, "IO Exception");
            }
        } else {
            Log.e(TAG, "Error opening media card in read/write mode!");
        }
        return result;
    }

    public static boolean saveLocation(Context context, Location location) {
        boolean result = false;
        if (isReadWrite()) {
            try {
                File outputFile = new File(context.getExternalFilesDir(null), location.getIdentifier());
                FileOutputStream outputStream = new FileOutputStream(outputFile);
                outputStream.write(location.getBytes());
                outputStream.close();
                result = true;
            } catch (FileNotFoundException e) {
                Log.e(TAG, "File not found");
            } catch (IOException e) {
                Log.e(TAG, "IO Exception");
            }
        } else {
            Log.e(TAG, "Error opening media card in read/write  mode!");
        }
        return result;
    }

    private static boolean isReadOnly() {
        Log.e(TAG, Environment.getExternalStorageState());
        return Environment.MEDIA_MOUNTED_READ_ONLY.equals(Environment.getExternalStorageState());
    }

    private static boolean isReadWrite() {
        Log.e(TAG, Environment.getExternalStorageState());
        return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
    }
}
