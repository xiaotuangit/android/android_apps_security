EncryptProxim应用程序

功能说明
    （1）在保存位置信息和联系信息前加密数据。

应用程序说明
    （1）使用SecretKeySpec和Cipher对数据进行AES加密：
        public static byte[] encrypt(byte[] key, byte[] data) {
            SecretKeySpec sKeySpec = new SecretKeySpec(key, "AES");
            Cipher cipher;
            byte[] ciphertext = null;
            try {
                cipher = Cipher.getInstance("AES");
                cipher.init(Cipher.ENCRYPT_MODE, sKeySpec);
                ciphertext = cipher.doFinal(data);
            } catch (NoSuchAlgorithmException e) {
                Log.e(TAG, "NoSuchAlgorithmException");
            } catch (NoSuchPaddingException e) {
                Log.e(TAG, "NoSuchPaddingException");
            } catch (IllegalBlockSizeException e) {
                Log.e(TAG, "IllegalBlockSizeException");
            } catch (BadPaddingException e) {
                Log.e(TAG, "BadPaddingException");
            } catch (InvalidKeyException e) {
                Log.e(TAG, "InvalidKeyException");
            }
            return ciphertext;
        }
    
    （2）使用KeyGenerator生成随机密钥：
        public static byte[] generateKey(byte[] randomNumberSeed) {
            SecretKey sKey = null;
            try {
                KeyGenerator keyGen = KeyGenerator.getInstance("AES");
                SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
                random.setSeed(randomNumberSeed);
                keyGen.init(256, random);
                sKey = keyGen.generateKey();
            } catch (NoSuchAlgorithmException e) {
                Log.e(TAG, "No such algorithm exception");
            }
            return sKey.getEncoded();
        }