package com.qty.encryptproxim;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class EncryptProxim extends Activity implements View.OnClickListener {

    private static final String TAG = "EncryptProxim";

    private EditText mFirstNameEt;
    private EditText mLastNameEt;
    private EditText mPhoneEt;
    private EditText mAddress1Et;
    private EditText mAddress2Et;
    private EditText mEmailEt;
    private EditText mIdentifierEt;
    private EditText mLatitudeEt;
    private EditText mLongitudeEt;
    private Button mSaveBt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encrypt_proxim);

        mFirstNameEt = (EditText) findViewById(R.id.first_name);
        mLastNameEt = (EditText) findViewById(R.id.last_name);
        mPhoneEt = (EditText) findViewById(R.id.phone);
        mAddress1Et = (EditText) findViewById(R.id.address1);
        mAddress2Et = (EditText) findViewById(R.id.address2);
        mEmailEt = (EditText) findViewById(R.id.email);
        mIdentifierEt = (EditText) findViewById(R.id.identifier);
        mLatitudeEt = (EditText) findViewById(R.id.latitude);
        mLongitudeEt = (EditText) findViewById(R.id.longitude);
        mSaveBt = (Button) findViewById(R.id.save);

        mSaveBt.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String firstName = mFirstNameEt.getText().toString();
        if (TextUtils.isEmpty(firstName)) {
            Toast.makeText(this, "First Name not allow empty", Toast.LENGTH_SHORT).show();
            return;
        }
        String lastName = mLastNameEt.getText().toString();
        if (TextUtils.isEmpty(lastName)) {
            Toast.makeText(this, "Last Name not allow empty", Toast.LENGTH_SHORT).show();
            return;
        }
        String phone = mPhoneEt.getText().toString();
        if (TextUtils.isEmpty(phone)) {
            Toast.makeText(this, "Phone not allow empty", Toast.LENGTH_SHORT).show();
            return;
        }
        String address1 = mAddress1Et.getText().toString();
        if (TextUtils.isEmpty(address1)) {
            Toast.makeText(this, "Address1 not allow empty", Toast.LENGTH_SHORT).show();
            return;
        }
        String address2 = mAddress2Et.getText().toString();
        if (TextUtils.isEmpty(address2)) {
            Toast.makeText(this, "Address2 not allow empty", Toast.LENGTH_SHORT).show();
            return;
        }
        String email = mEmailEt.getText().toString();
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, "Email not allow empty", Toast.LENGTH_SHORT).show();
            return;
        }
        String identifier = mIdentifierEt.getText().toString();
        if (TextUtils.isEmpty(identifier)) {
            Toast.makeText(this, "Identifier not allow empty", Toast.LENGTH_SHORT).show();
            return;
        }
        String latitudeStr = mLatitudeEt.getText().toString();
        double latitude = 0.0;
        try {
            latitude = Double.parseDouble(latitudeStr);
        } catch (Exception e) {
            Toast.makeText(this, "Illegal latitude", Toast.LENGTH_SHORT).show();
            return;
        }
        String longitudeStr = mLongitudeEt.getText().toString();
        double longitude = 0.0;
        try {
            longitude = Double.parseDouble(longitudeStr);
        } catch (Exception e) {
            Toast.makeText(this, "Illegal longitude", Toast.LENGTH_SHORT).show();
            return;
        }
        Contact contact = new Contact();
        contact.setFirstName(firstName);
        contact.setLastName(lastName);
        contact.setAddress1(address1);
        contact.setAddress2(address2);
        contact.setPhone(phone);
        contact.setEmail(email);

        Location location = new Location();
        location.setIdentifier(identifier);
        location.setLatitude(latitude);
        location.setLongitude(longitude);

        if (SaveController.saveLocation(this, location)
                && SaveController.saveContact(this, contact)) {
            Toast.makeText(this, "Save success", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Save fail", Toast.LENGTH_SHORT).show();
        }
    }

}
