RESTFetch

应用功能说明
    通过HTTP GET获取服务器反馈的数据库数据，并通过DOC方式解析返回的xml文件

应用技术要点说明：
    （1）HTTP GET请求， 主要代码如下：
        BufferedReader in = null;
        StringBuffer sb = new StringBuffer("");

        try {
            HttpClient client = new DefaultHttpClient();
            HttpGet request = new HttpGet();
            request.setURI(new URI("http://192.168.3.105/apress/members"));
            HttpResponse response = client.execute(request);
            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            String newLine = System.getProperty("line.separator");
            while ((line = in.readLine()) != null) {
                sb.append(line + newLine);
            }
            in.close();
        } catch (IOException e) {
            Log.e(TAG, "doInBackground=>error: ", e);
        } catch (URISyntaxException e) {
            Log.e(TAG, "doInBackground=>error: ", e);
        }

    （2）DOM解析XML文件：
        try {
            StringBuffer stringBuffer = new StringBuffer("");
            Document doc = null;
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(stringBuffer.toString()));
            doc = db.parse(is);

            NodeList nodes = doc.getElementsByTagName("user");
            String[] names = new String[nodes.getLength()];
            for (int k = 0; k < nodes.getLength(); ++k) {
                names[k] = nodes.item(k).getAttributes().getNamedItem("name").getNodeValue();
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();S
        } catch (SAXException e) {
            e.printStackTrace();
        }