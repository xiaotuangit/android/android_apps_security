package com.qty.restfetch;

import android.app.Activity;
import android.app.ListActivity;
import android.net.http.HttpResponseCache;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class RESTFetchActivity extends ListActivity {

    private static final String TAG = "RESTFetch";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String[] names = new String[]{};

        setListAdapter(new ArrayAdapter<String>(this, R.layout.list_item, names));

        ListView lv = getListView();
        lv.setTextFilterEnabled(true);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getApplicationContext(), ((TextView) view).getText(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        new RestFetchTask().execute(new Void[]{});
    }

    private class RestFetchTask extends AsyncTask<Void, Void, StringBuffer> {

        @Override
        protected StringBuffer doInBackground(Void... voids) {
            BufferedReader in = null;
            StringBuffer sb = new StringBuffer("");

            try {
                HttpClient client = new DefaultHttpClient();
                HttpGet request = new HttpGet();
                request.setURI(new URI("http://192.168.3.105/apress/members"));
                HttpResponse response = client.execute(request);
                in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                String line = "";
                String newLine = System.getProperty("line.separator");
                while ((line = in.readLine()) != null) {
                    sb.append(line + newLine);
                }
                in.close();
            } catch (IOException e) {
                Log.e(TAG, "doInBackground=>error: ", e);
            } catch (URISyntaxException e) {
                Log.e(TAG, "doInBackground=>error: ", e);
            }
            return sb;
        }

        @Override
        protected void onPostExecute(StringBuffer stringBuffer) {
            super.onPostExecute(stringBuffer);
            try {
                Document doc = null;
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(stringBuffer.toString()));
                doc = db.parse(is);

                NodeList nodes = doc.getElementsByTagName("user");
                String[] names = new String[nodes.getLength()];
                for (int k = 0; k < nodes.getLength(); ++k) {
                    names[k] = nodes.item(k).getAttributes().getNamedItem("name").getNodeValue();
                }

                setListAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.list_item, names));

                ListView lv = getListView();
                lv.setTextFilterEnabled(true);

                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Toast.makeText(getApplicationContext(), ((TextView) view).getText(), Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            }
        }
    }
}
