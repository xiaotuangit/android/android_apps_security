MySQLConnect

应用功能说明
    连接远程MySQL服务器，并查询数据库中的数据。

应用技术要点说明
    （1）使用JDBC连接远程MySQL数据库，并查询数据库数据，主要代码如下：
        Connection conn = null;
        String host = "192.168.3.105";
        int port = 3306;
        String db = "android";

        String user = "sheran";
        String pass = "P@ssw0rd";

        String url = "jdbc:mysql://" + host + ":" + port + "/" + db + "?user=" + user + "&password=" + pass;
        String sql = "SELECT * FROM apress";

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection(url);

            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            Hashtable<String, String> details = new Hashtable<String, String>();
            while (rs.next()) {
                details.put(rs.getString("name"), rs.getString("email"));
            }
            conn.close();
        } catch (IllegalAccessException e) {
            Log.e(TAG, "Illegal access error: ", e);
        } catch (InstantiationException e) {
            Log.e(TAG, "Instantiation error " + e.getMessage());
        } catch (SQLException e) {
            Log.e(TAG, "SQL Exception " + e.getMessage());
        } catch (ClassNotFoundException e) {
            Log.e(TAG, "Class not found!");
        }

    （2）需要下载MySQL的jdbc库才可以运行上面的代码，下载地址： www.mysql.com/products/connector/