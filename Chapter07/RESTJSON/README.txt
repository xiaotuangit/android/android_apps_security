RestJson

应用功能说明
    通过HTTP GET请求数据库信息，并解析JSON数据

应用技术要点说明：
    （1）HTTP GET请求数据，主要代码如下：
        BufferedReader in = null;
        StringBuffer sb = new StringBuffer("");

        try {
            HttpClient client = new DefaultHttpClient();
            HttpGet request = new HttpGet();
            request.setURI(new URI("http://192.168.3.105/apress/members.json"));
            HttpResponse response = client.execute(request);
            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            while ((line = in.readLine()) != null) {
                sb.append(line);
            }
            in.close();
        } catch (IOException e) {
            Log.e(TAG, "doInBackground=>error: ", e);
        } catch (URISyntaxException e) {
            Log.e(TAG, "doInBackground=>error: ", e);
        }

    （2）解析JSON数据，主要代码如下：
        try {
            String s = "";
            JSONObject users = new JSONObject(s.toString()).getJSONObject("users");
            JSONArray jArray = users.getJSONArray("user");
            String[] names = new String[jArray.length()];
            for (int i = 0; i < jArray.length(); i++) {
                JSONObject jsonObject = jArray.getJSONObject(i);
                names[i] = jsonObject.getString("name");
            }
        } catch (JSONException e) {
            Log.e(TAG, "onPostExecute=>error: ", e);
        }