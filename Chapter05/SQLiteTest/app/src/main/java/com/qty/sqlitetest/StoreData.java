package com.qty.sqlitetest;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

public class StoreData {

    public static long store(ContactsDb db, Contact contact) {
        // Prepare values
        ContentValues values = new ContentValues();
        values.put("FIRSTNAME", contact.getFirstName());
        values.put("LASTNAME", contact.getLastName());
        values.put("EMAIL", contact.getEmail());
        values.put("PHONE", contact.getPhone());
        values.put("ADDRESS1", contact.getAddress1());
        values.put("ADDRESS2", contact.getAddress2());

        SQLiteDatabase wdb = db.getWritableDatabase();
        return wdb.insert(db.tblName, null, values);
    }

}
