package com.qty.sqlitetest;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class RetrieveData {

    public static Contact get(ContactsDb db) {
        SQLiteDatabase rdb = db.getReadableDatabase();
        String[] cols = { "FIRSTNAME", "LASTNAME", "EMAIL", "PHONE" };
        Cursor results = rdb.query(db.tblName, cols, "", null, "", "", "");

        Contact c = new Contact();
        results.moveToLast();
        c.setFirstName(results.getString(results.getColumnIndex("FIRSTNAME")));
        c.setLastName(results.getString(results.getColumnIndex("LASTNAME")));
        c.setEmail(results.getString(results.getColumnIndex("EMAIL")));
        c.setPhone(results.getString(results.getColumnIndex("PHONE")));
        return c;
    }
}
