package com.qty.sqlitetest;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

import java.util.concurrent.locks.ReadWriteLock;

public class SQLiteTestActivity extends Activity {

    /** Called when the activity is first created. */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sqlite_test);

        // Store data
        Contact contact = new Contact();
        contact.setFirstName("Sheran");
        contact.setLastName("Gunasekera");
        contact.setEmail("sheran@zenconsult.net");
        contact.setPhone("+12120031337");

        ContactsDb db = new ContactsDb(getApplicationContext(), "ContactDb", null, 1);
        Log.i("SQLiteTextActivity", String.valueOf(StoreData.store(db, contact)));

        Contact c = RetrieveData.get(db);

        db.close();

        EditText ed = (EditText) findViewById(R.id.content);
        ed.setText(contact.toString());
    }
}
