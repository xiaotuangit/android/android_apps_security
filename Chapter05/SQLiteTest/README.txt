SQLiteTest

应用功能说明
    演示使用SQLite存储数据和读取数据

应用技术要点说明
    （1）创建数据库，主要代码如下：
        import android.content.Context;
        import android.database.sqlite.SQLiteDatabase;
        import android.database.sqlite.SQLiteOpenHelper;

        public class ContactsDb extends SQLiteOpenHelper {

            public static final String tblName = "Contacts";

            public ContactsDb(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
                super(context, name, factory, version);
            }

            @Override
            public void onCreate(SQLiteDatabase db) {
                String createSQL = "CREATE TABLE " + tblName + " ( FIRSTNAME TEXT, LASTNAME TEXT, EMAIL TEXT,"
                        + " PHONE TEXT, ADDRESS1 TEXT, ADDRESS2 TEXT);";
                db.execSQL(createSQL);
            }

            @Override
            public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
                // Use this to handle upgradedc versions of your database
            }
        }

    （2）创建ContactsDb对象：
        ContactsDb db = new ContactsDb(getApplicationContext(), "ContactDb", null, 1);

    （3）使用SQLiteDatabase存储数据，主要代码如下：
        public static long store(ContactsDb db, Contact contact) {
            // Prepare values
            ContentValues values = new ContentValues();
            values.put("FIRSTNAME", contact.getFirstName());
            values.put("LASTNAME", contact.getLastName());
            values.put("EMAIL", contact.getEmail());
            values.put("PHONE", contact.getPhone());
            values.put("ADDRESS1", contact.getAddress1());
            values.put("ADDRESS2", contact.getAddress2());

            SQLiteDatabase wdb = db.getWritableDatabase();
            return wdb.insert(db.tblName, null, values);
        }

    （4）读取SQLiteDatabase存储的数据， 主要代码如下：
        public static Contact get(ContactsDb db) {
            SQLiteDatabase rdb = db.getReadableDatabase();
            String[] cols = { "FIRSTNAME", "LASTNAME", "EMAIL", "PHONE" };
            Cursor results = rdb.query(db.tblName, cols, "", null, "", "", "");

            Contact c = new Contact();
            results.moveToLast();
            c.setFirstName(results.getString(results.getColumnIndex("FIRSTNAME")));
            c.setLastName(results.getString(results.getColumnIndex("LASTNAME")));
            c.setEmail(results.getString(results.getColumnIndex("EMAIL")));
            c.setPhone(results.getString(results.getColumnIndex("PHONE")));
            return c;
        }