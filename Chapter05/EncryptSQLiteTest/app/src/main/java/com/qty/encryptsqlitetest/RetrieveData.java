package com.qty.encryptsqlitetest;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class RetrieveData {

    private static final String TAG = "RetrieveData";

    public static Contact get(Crypto crypto, ContactsDb db) {
        SQLiteDatabase rdb = db.getReadableDatabase();
        String[] cols = { "FIRSTNAME", "LASTNAME", "EMAIL", "PHONE" };
        Cursor results = rdb.query(ContactsDb.tblName, cols, "", null, "", "", "");

        Contact c = new Contact();
        results.moveToLast();

        try {
            c.setFirstName(crypto.armorDecrypt(results.getString(results.getColumnIndex("FIRSTNAME"))));
            c.setLastName(crypto.armorDecrypt(results.getString(results.getColumnIndex("LASTNAME"))));
            c.setEmail(crypto.armorDecrypt(results.getString(results.getColumnIndex("EMAIL"))));
            c.setPhone(crypto.armorDecrypt(results.getString(results.getColumnIndex("PHONE"))));
        } catch (NoSuchPaddingException e) {
            Log.e(TAG, "Exception in RetrieveData: " + e.getMessage());
        } catch (InvalidAlgorithmParameterException e) {
            Log.e(TAG, "Exception in RetrieveData: " + e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "Exception in RetrieveData: " + e.getMessage());
        } catch (IllegalBlockSizeException e) {
            Log.e(TAG, "Exception in RetrieveData: " + e.getMessage());
        } catch (BadPaddingException e) {
            Log.e(TAG, "Exception in RetrieveData: " + e.getMessage());
        } catch (InvalidKeyException e) {
            Log.e(TAG, "Exception in RetrieveData: " + e.getMessage());
        }
        return c;
    }
}
