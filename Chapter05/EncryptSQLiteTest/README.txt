EncryptSQLiteTest

应用功能说明
    在存储数据到数据库前加密数据，在读取数据库数据后进行解密

应用技术要点说明
    （1）加密解密帮助类，代码如下：
        import android.content.Context;
        import android.util.Base64;

        import java.security.InvalidAlgorithmParameterException;
        import java.security.InvalidKeyException;
        import java.security.NoSuchAlgorithmException;

        import javax.crypto.BadPaddingException;
        import javax.crypto.Cipher;
        import javax.crypto.IllegalBlockSizeException;
        import javax.crypto.NoSuchPaddingException;
        import javax.crypto.spec.IvParameterSpec;
        import javax.crypto.spec.SecretKeySpec;

        public class Crypto {

            private static final String engine = "AES";
            private static final String crypto = "AES/CBC/PKCS5Padding";
            private static Context ctx;

            public Crypto(Context cntx) {
                ctx = cntx;
            }

            public byte[] cipher(byte[] data, int mode) throws NoSuchPaddingException,
                    NoSuchAlgorithmException, InvalidAlgorithmParameterException,
                    InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
                KeyManager km = new KeyManager(ctx);
                SecretKeySpec sks = new SecretKeySpec(km.getId(), engine);
                IvParameterSpec iv = new IvParameterSpec(km.getIv());
                Cipher c = Cipher.getInstance(crypto);
                c.init(mode, sks, iv);
                return c.doFinal(data);
            }

            public byte[] encrypt(byte[] data) throws NoSuchPaddingException, InvalidKeyException,
                    NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException,
                    InvalidAlgorithmParameterException {
                return cipher(data, Cipher.ENCRYPT_MODE);
            }

            public byte[] decrypt(byte[] data) throws NoSuchPaddingException, InvalidKeyException,
                    NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException,
                    InvalidAlgorithmParameterException {
                return cipher(data, Cipher.DECRYPT_MODE);
            }

            public String armorEncrypt(byte[] data) throws NoSuchPaddingException,
                    InvalidAlgorithmParameterException, NoSuchAlgorithmException,
                    IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
                return Base64.encodeToString(encrypt(data), Base64.DEFAULT);
            }

            public String armorDecrypt(String data) throws NoSuchPaddingException,
                    InvalidAlgorithmParameterException, NoSuchAlgorithmException,
                    IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
                return new String(decrypt(Base64.decode(data, Base64.DEFAULT)));
            }
        }

    （2）存储数据时加密数据，主要代码如下：
        public static long store(Crypto crypto, ContactsDb db, Contact contact) {
            // Prepare values
            ContentValues values = new ContentValues();
            try {
                values.put("FIRSTNAME", crypto.armorEncrypt(contact.getFirstName().getBytes()));
                values.put("LASTNAME", crypto.armorEncrypt(contact.getLastName().getBytes()));
                values.put("EMAIL", crypto.armorEncrypt(contact.getEmail().getBytes()));
                values.put("PHONE", crypto.armorEncrypt(contact.getPhone().getBytes()));
                values.put("ADDRESS1", contact.getAddress1());
                values.put("ADDRESS2", contact.getAddress2());
            } catch (NoSuchPaddingException e) {
                Log.e(TAG, "Exception in StoreData: " + e.getMessage());
            } catch (InvalidAlgorithmParameterException e) {
                Log.e(TAG, "Exception in StoreData: " + e.getMessage());
            } catch (NoSuchAlgorithmException e) {
                Log.e(TAG, "Exception in StoreData: " + e.getMessage());
            } catch (IllegalBlockSizeException e) {
                Log.e(TAG, "Exception in StoreData: " + e.getMessage());
            } catch (BadPaddingException e) {
                Log.e(TAG, "Exception in StoreData: " + e.getMessage());
            } catch (InvalidKeyException e) {
                Log.e(TAG, "Exception in StoreData: " + e.getMessage());
            }
            SQLiteDatabase wdb = db.getWritableDatabase();
            return wdb.insert(db.tblName, null, values);
        }

    （3）读取数据后对数据进行解密，主要代码如下：
        public static Contact get(Crypto crypto, ContactsDb db) {
            SQLiteDatabase rdb = db.getReadableDatabase();
            String[] cols = { "FIRSTNAME", "LASTNAME", "EMAIL", "PHONE" };
            Cursor results = rdb.query(ContactsDb.tblName, cols, "", null, "", "", "");

            Contact c = new Contact();
            results.moveToLast();

            try {
                c.setFirstName(crypto.armorDecrypt(results.getString(results.getColumnIndex("FIRSTNAME"))));
                c.setLastName(crypto.armorDecrypt(results.getString(results.getColumnIndex("LASTNAME"))));
                c.setEmail(crypto.armorDecrypt(results.getString(results.getColumnIndex("EMAIL"))));
                c.setPhone(crypto.armorDecrypt(results.getString(results.getColumnIndex("PHONE"))));
            } catch (NoSuchPaddingException e) {
                Log.e(TAG, "Exception in RetrieveData: " + e.getMessage());
            } catch (InvalidAlgorithmParameterException e) {
                Log.e(TAG, "Exception in RetrieveData: " + e.getMessage());
            } catch (NoSuchAlgorithmException e) {
                Log.e(TAG, "Exception in RetrieveData: " + e.getMessage());
            } catch (IllegalBlockSizeException e) {
                Log.e(TAG, "Exception in RetrieveData: " + e.getMessage());
            } catch (BadPaddingException e) {
                Log.e(TAG, "Exception in RetrieveData: " + e.getMessage());
            } catch (InvalidKeyException e) {
                Log.e(TAG, "Exception in RetrieveData: " + e.getMessage());
            }
            return c;
        }

    （4）需要注意本示例使用AES/CBC/PKCS5Padding算法进行加密，在设置密码时，
密码长度是有限制的，具体长度限制可以自行尝试，本例使用如下密码长度：
        String key = "12345678909876543212345678909876";
        String iv = "1234567890987654";