package com.qty.internalstoragetest;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.EditText;

import java.util.concurrent.locks.ReadWriteLock;

public class InternalStorageActivity extends Activity {

    /** Called when the activity is first created. */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_internal_storage);

        Context ctx = getApplicationContext();

        // Store data
        Contact contact = new Contact();
        contact.setFirstName("Sheran");
        contact.setLastName("Gunasekera");
        contact.setEmail("sheran@zenconsult.net");
        contact.setPhone("+12120031337");

        StoreData.storeData(contact.getBytes(), ctx);

        // Retrieve data

        EditText ed = (EditText) findViewById(R.id.content);
        ed.setText(new String(RetrieveData.get(ctx)));
    }
}
