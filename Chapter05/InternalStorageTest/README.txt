InternalStorageTest

应用功能说明
    使用Internal Storage存储和读取数据

应用技术要点说明：
    （1）使用Internal Storage存储数据， 主要代码如下：
        public static void storeData(byte[] data, Context ctx) {
            try {
                FileOutputStream fos = ctx.openFileOutput(file, ctx.MODE_PRIVATE);
                fos.write(data);
                fos.close();
            } catch (FileNotFoundException e) {
                Log.e("StoreData", "Exception: " + e.getMessage());
            } catch (IOException e) {
                Log.e("StoreData", "Exception: " + e.getMessage());
            }
        }

    （2）读取Interanl Storage存储的数据，主要代码如下：
        public static byte[] get(Context ctx) {
            byte[] data = null;
            try {
                int bytesRead = 0;
                FileInputStream fis = ctx.openFileInput(file);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                byte[] b = new byte[1024];
                while ((bytesRead = fis.read(b)) != -1) {
                    bos.write(b, 0, bytesRead);
                }
                data = bos.toByteArray();
            } catch (FileNotFoundException e) {
                Log.e("RetrieveData", "Exception: " + e.getMessage());
            } catch (IOException e) {
                Log.e("RetrieveData", "Exception: " + e.getMessage());
            }
            return data;
        }