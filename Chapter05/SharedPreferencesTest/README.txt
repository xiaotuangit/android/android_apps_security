SharedPreferencesTest

应用功能说明
    演示使用SharedPreferences存储和读取数据。

应用技术要点：
    （1）使用SharedPreferences存储数据，主要代码如下：
        public static boolean storeData(Hashtable data, Context ctx) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
            String hostname = (String) data.get("hostname");
            int port = (Integer) data.get("port");
            boolean useSSL = (Boolean) data.get("ssl");
            SharedPreferences.Editor ed = prefs.edit();
            ed.putString("hostname", hostname);
            ed.putInt("port", port);
            ed.putBoolean("ssl", useSSL);
            return ed.commit();
        }

    （2）读取SharedPreferences存储的数据，主要代码如下：
        public static Hashtable get(Context ctx) {
            String hostname = "hostname";
            String port = "port";
            String ssl = "ssl";
            Hashtable data = new Hashtable();
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
            data.put(hostname, prefs.getString(hostname, null));
            data.put(port, prefs.getInt(port, 0));
            data.put(ssl, prefs.getBoolean(ssl, true));
            return data;
        }