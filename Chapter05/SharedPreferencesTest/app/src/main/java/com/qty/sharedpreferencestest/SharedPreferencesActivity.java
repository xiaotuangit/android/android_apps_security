package com.qty.sharedpreferencestest;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

import java.util.Hashtable;

public class SharedPreferencesActivity extends Activity {

    /** Called when the activity is first created. */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_preferences);

        Context cntxt = getApplicationContext();

        Hashtable data = new Hashtable();
        data.put("hostname", "smtp.gmail.com");
        data.put("port", 587);
        data.put("ssl", true);

        if (StoreData.storeData(data, cntxt)) {
            Log.i("SE", "Successfully wrote data");
        } else {
            Log.e("SE", "Failed to write data to Shared Prefs");
        }

        EditText ed = (EditText) findViewById(R.id.content);
        ed.setText(RetrieveData.get(cntxt).toString());
        Log.d("SE", "data: " + RetrieveData.get(cntxt).toString());
    }
}
