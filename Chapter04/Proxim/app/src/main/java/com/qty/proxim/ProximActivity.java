package com.qty.proxim;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.qty.proxim.controller.SaveController;
import com.qty.proxim.model.Contact;

public class ProximActivity extends Activity {

    /** Called when the activity is first created. */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proxim);

        final Contact contact = new Contact();
        contact.setFirstName("Sheran");
        contact.setLastName("Gunasekera");
        contact.setAddress1("");
        contact.setAddress2("");
        contact.setEmail("sheran@zenconsult.net");
        contact.setPhone("12120031337");
        final Button button = (Button) findViewById(R.id.button1);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveController.saveContact(getApplicationContext(), contact);
            }
        });
    }
}
