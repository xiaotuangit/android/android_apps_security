package com.qty.loginclient;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LoginClientActivity extends Activity implements View.OnClickListener {

    private final String TAG = "LoginClient";

    private HttpResponse response;
    private Login login;

    /** Called when the activity is first created. */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_client);

        Button button = (Button) findViewById(R.id.login);
        button.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        String u = ((EditText) findViewById(R.id.username)).toString();
        String p = ((EditText) findViewById(R.id.password)).toString();

        login = new Login(u, p);

        final EditText text = (EditText) findViewById(R.id.editText1);
        text.setText("");

        new LoginTask().execute(new Void[]{});
    }

    private class LoginTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... voids) {
            String msg = "";

            response = login.execute();
            Log.i(TAG, "After login.execute()");

            if (response != null) {
                if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                        StringBuilder sb = new StringBuilder();
                        String line;
                        while ((line = reader.readLine()) != null) {
                            sb.append(line);
                        }
                        msg = sb.toString();
                    } catch (IOException e) {
                        Log.e(TAG, "IO Exception in reading from stream.");
                    }
                } else {
                    msg = "Status code other than HTTP 200 received";
                }
            } else {
                msg = "Response is null";
            }
            Log.d(TAG, "doInBackground=>msg: " + msg);
            return msg;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            final EditText text = (EditText) findViewById(R.id.editText1);
            text.setText(s);
        }
    }
}
