LoginClient

应用功能说明
    通过HttpClient进行post登录请求，并显示服务器返回信息

应用技术要点说明
    （1）使用HttpClient进行Post请求，主要代码如下：
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost("http://logindemo1.appspot.com/logindemo");
        HttpResponse response = null;

        // Post data with number of parameters
        List<NameValuePair> nvPairs = new ArrayList<NameValuePair>(2);
        nvPairs.add(new BasicNameValuePair("username", username));
        nvPairs.add(new BasicNameValuePair("password", password));

        // Add post data to http post
        try {
            UrlEncodedFormEntity params = new UrlEncodedFormEntity(nvPairs);
            post.setEntity(params);
            response = client.execute(post);
            Log.i(TAG, "After client.execute()");
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "Umnsupported Encoding used");
        } catch (ClientProtocolException e) {
            Log.e(TAG, "Client Protocol Exception");
        } catch (IOException e) {
            Log.e(TAG, "IOException in HttpPost");
        }

    （2）在非主线程执行网络请求，主要代码如下：
        private class LoginTask extends AsyncTask<Void, Void, String> {

            @Override
            protected String doInBackground(Void... voids) {
                String msg = "";

                response = login.execute();
                Log.i(TAG, "After login.execute()");

                if (response != null) {
                    if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                        try {
                            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                            StringBuilder sb = new StringBuilder();
                            String line;
                            while ((line = reader.readLine()) != null) {
                                sb.append(line);
                            }
                            msg = sb.toString();
                        } catch (IOException e) {
                            Log.e(TAG, "IO Exception in reading from stream.");
                        }
                    } else {
                        msg = "Status code other than HTTP 200 received";
                    }
                } else {
                    msg = "Response is null";
                }
                Log.d(TAG, "doInBackground=>msg: " + msg);
                return msg;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                final EditText text = (EditText) findViewById(R.id.editText1);
                text.setText(s);
            }
        }

    （3）需要注意的是，在SDK 23及以上不再支持org.apache.http，需要降低到SDK 22版本及以下。
    或者通过下载HttpClient jar库，将其添加到工程中即可。