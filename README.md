# android_apps_security

Android Apps Security书籍配套源代码

第1章 Android架构
        1. VideoViewDemo                                ===> 演示使用应用框架播放视频

第2章 信息：应用程序的基础
        1. Proxim                                       ===> 保存位置信息和联系信息到本地存储
        2. EncryptProxim                                ===> 在保存位置信息和联系信息到本地存储前加密数据

第3章 Android安全体系架构
        1. ZenLibrary                                   ===> 自定义权限

第4章 实操1
        1. Proxim                                       ===> 实现第2章的Proxim完整应用

第5章 数据存储与密码学
        1. SharedPreferencesTest                        ===> 使用SharedPreferences存储和读取数据
        2. InternalStorageTest                          ===> 使用Internal Storage存储和读取数据
        3. SQLiteTest                                   ===> 使用SQLite数据库存储和读取数据
        4. EncryptSQLiteTest                            ===> 将数据存储到数据前加密数据，读取数据库时解密数据

第6章 谈谈Web应用程序
        1. LoginDemo                                    ===> Login网页客户端代码
        2. LoginClient                                  ===> 使用HttpClient进行Post登录

第7章 企业安全
        1. MySQLConnect                                 ===> 使用jdbc连接远程MySQL数据库，并查询数据库数据
        2. RESTFetch                                    ===> 通过HTTP GET获取数据库数据，并使用DOM解析XML文件
        3. RestJson                                     ===> 通过HTTP GET获取数据库数据，并解析JSON数据

第8章 实操2
        1. OAuthPicasa                                  ===> 演示OAuth使用流程
        2. ChallengeResponseClient                      ===> 演示客户端基于挑战/响应的流程
        3. ChallengeResponse                            ===> 基于挑战/响应流程的部分服务器代码

第9章 发布和销售应用
        1. ChuckNorrisFacts                             ===> 使用Android License Verification Library对应用程序进行验证
        2. LicenseCheck                                 ===> 演示未使用Android License Verification Library对应用程序进行验证的示例