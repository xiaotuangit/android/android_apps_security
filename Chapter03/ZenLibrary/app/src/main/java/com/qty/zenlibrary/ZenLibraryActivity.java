package com.qty.zenlibrary;

import android.app.Activity;
import android.os.Bundle;

public class ZenLibraryActivity extends Activity {

    /** Called when the activity is first created. */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zen_library);
    }
}
