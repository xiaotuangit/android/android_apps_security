ZenLibrary

应用说明
    （1）自定义权限

功能说明
（1）自定义权限代码：
    <permission android:name="net.zenconsult.libs.Mofest.permission.PURGE_DATABASE"
        android:protectionLevel="dangerous"
        android:label="@string/label_purgeDatabase"
        android:description="@string/description_purgeDatabase"
        android:permissionGroup="android.permission-group.COST_MONEY" />

    android:name                    权限名称
    android:protectionLevel         保护等级
    android:description             权限描述
    android:permissionGroup         所属的权限组

（2）请求自定义权限代码：
    <uses-permission android:name="net.zenconsult.libs.Mofest.permission.PURGE_DATABASE" />

（3）为组件设置权限：
    <activity android:name=".ZenLibraryActivity"
        android:permission="net.zenconsult.libs.Mofest.permission.PURGE_DATABASE" >
        <intent-filter>
            <action android:name="android.intent.action.MAIN" />

            <category android:name="android.intent.category.LAUNCHER" />
        </intent-filter>
    </activity>

（4）注意：在这个示例代码中应用程序定义自定义权限的同时请求自定义权限，这样的做法是无效的。安装这个APK后，
即使授予应用该自定义权限，点击桌面应用图标也会提示应用未安装提示，这是因为Home应用没有请求自定义权限的缘故。
还有就是最好先安装自定义权限的应用，然后才安装请求该权限的应用，否则可能会遇到麻烦。