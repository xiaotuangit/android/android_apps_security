package net.zenconsult.android;

import java.io.StringWriter;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.codec.binary.Base64;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

public class CRAM implements Constants {

    private final byte[] secret = new byte[32];

    public CRAM() {
        SecureRandom sr = new SecureRandom();
        sr.nextBytes(secret);
    }

    public String generate() {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        try {
            dBuilder = dbFactory.newDocumentBuidler();
        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Document doc = dBuilder.newDocument();

        // Build Root
        Element root = doc.createElement("ServerResponse");
        doc.appendChild(root);

        // Challenge Section
        Element authChallenge = doc.createElement("AuthChallenge");
        root.appendChild(authChallenge);

        // The Challenge
        Element challenge = doc.createElement("Challenge");
        Text challengeText = doc.createTextNode(Base64.encodeBase64String(secret));
        challenge.appendChild(challengeText);
        authChallenge.appendChild(challenge);

        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer = null;
        try {
            transformer = tFactory.newTransformer();
        } catch (TransformerConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        StringWriter sw = new StringWriter();
        StreamResult res = new StreamResult(sw);
        DOMSource source = new DOMSource(doc);
        try {
            transformer.transform(source, res);
        } catch (TransformerException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String xml = sw.toString();
        return xml;
    }

    public boolean verifyChallenge(String userResponse) {
        String algo = "HmacSHA1";
        Mac mac = null;
        try {
            mac = Mac.getInstance(algo);
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated chatch block
            e.printStackTrace();
        }
        SecretKey key = new SecretKeySpec(PASSWORD.getBytes(), algo);

        try {
            mac.init(key);
        } catch (InvalidKeyException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String tmpHash = USERNAME + " " + Hex.toHex(mac.doFinal(secret));
        String hash = Base64.encodeBase64String(tmpHash.getBytes());
        return hash.equals(userResponse);
    }

    public String generateReply(String response) {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Document doc = dBuilder.newDocument();

        // Build Root
        Element root = doc.createElement("ServerResponse");
        doc.appendChild(root);

        // Challenge Section
        Element authChallenge = doc.createElement("AuthChallenge");
        root.appendChild(authChallenge);

        // Reply
        Element challenge = doc.createElement("Response");
        Text challengeText = doc.createTextNode(response);
        challenge.appendChild(challengeText);
        authChallenge.appendChild(challenge);

        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer = null;
        try {
            transformer = tFactory.newTransformer();
        } catch (TransformerConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        StringWriter sw = new StringWriter();
        StreamResult res = new StreamResult(sw);
        DOMSource source = new DOMSource(doc);
        try {
            transformer.transform(source, res);
        } catch (TransformerException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String xml = sw.toString();
        return xml;
    }
}