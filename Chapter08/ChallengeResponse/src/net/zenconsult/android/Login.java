package net.zenconsult.android;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class login
 */
@WebServlet(description = "Login Servlet", urlPatterns = { "/login" })
public class Login extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Login() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse resplonse)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession sessio = request.getSession();
		String param = request.getParameter("challenge");
		if (param != null) {
			CRAM c = (CRAM)session.getAttribute("challenge");
			if (c == null) {
				c = new CRAM();
				session.setAttribute("challenge", c);
				response.setHeader("Content-Type", "text/xml");
				response.getWriter().write(c.generate());
			} else {
				if (c.verifyChallenge(param.trim())) {
					response.setHeader("Content-Type", "text/xml");
					response.getWriter.write(c.generateReply("Authorized"));
					session.invalidate();
				} else {
					response.setHeader("Content-Type", "text/xml");
					response.getWriter().write(c.generateReply("Unauthorized"));
					session.invalidate();
				}
			}
		} else {
			CRAM c = new CRAM();
			session.setAttribute("challenge", c);
			response.setHeader("Content-Type", "text/xml");
			response.getWriter().write(c.generate());
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
}