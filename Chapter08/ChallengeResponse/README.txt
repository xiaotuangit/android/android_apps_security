ChallengeResponse

应用功能说明
    基于挑战/响应流程的部分服务器代码，该代码不完整。

应用技术说明
    （1）创建XML文件，主要代码如下：
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        try {
            dBuilder = dbFactory.newDocumentBuidler();
        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Document doc = dBuilder.newDocument();

        // Build Root
        Element root = doc.createElement("ServerResponse");
        doc.appendChild(root);

        // Challenge Section
        Element authChallenge = doc.createElement("AuthChallenge");
        root.appendChild(authChallenge);

        // The Challenge
        Element challenge = doc.createElement("Challenge");
        Text challengeText = doc.createTextNode(Base64.encodeBase64String(secret));
        challenge.appendChild(challengeText);
        authChallenge.appendChild(challenge);

        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer = null;
        try {
            transformer = tFactory.newTransformer();
        } catch (TransformerConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        StringWriter sw = new StringWriter();
        StreamResult res = new StreamResult(sw);
        DOMSource source = new DOMSource(doc);
        try {
            transformer.transform(source, res);
        } catch (TransformerException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String xml = sw.toString();
        return xml;