package com.qty.challengeresponseclient;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class ChallengeResponseClient extends Activity {

    private static final String TAG = "ChallengeResponse";

    /** Called when the activity is first created. */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge_response_client);

        Button button = (Button) findViewById(R.id.button1);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new LoginTask().execute(new Void[]{});
            }
        });
    }

    private class LoginTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            Comms c = new Comms(ChallengeResponseClient.this);
            String challenge = c.getChallenge();
            CRAM cram = new CRAM(ChallengeResponseClient.this);
            String hash = cram.generate(challenge);
            if (hash != null) {
                String reply = c.sendResponse(hash);
                return c.authorized(reply);
            } else {
                Log.e(TAG, "doInBackground=>hash is null.");
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (aBoolean) {
                Toast.makeText(ChallengeResponseClient.this, "Login success", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(ChallengeResponseClient.this, "Login failed", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
