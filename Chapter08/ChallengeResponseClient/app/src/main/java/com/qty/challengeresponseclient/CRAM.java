package com.qty.challengeresponseclient;

import android.app.Activity;
import android.util.Base64;
import android.util.Log;
import android.widget.TextView;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class CRAM {

    private static final String TAG = "CRAM";

    private Activity activity;

    public CRAM(Activity act) {
        activity = act;
    }

    public String generate(String serverChallenge) {
        String algo = "HmacSHA1";
        TextView pass = (TextView) activity.findViewById(R.id.editText2);
        byte[] server = Base64.decode(serverChallenge, Base64.DEFAULT);

        Mac mac = null;
        try {
            mac = Mac.getInstance(algo);
            String keyText = pass.getText().toString();
            SecretKey key = new SecretKeySpec(keyText.getBytes(), algo);
            mac.init(key);
            byte[] tmpHash = mac.doFinal(server);
            TextView user = (TextView) activity.findViewById(R.id.editText1);
            String username = user.getText().toString();
            String concat = username + " " + Hex.toHex(tmpHash);
            return Base64.encodeToString(concat.getBytes(), Base64.URL_SAFE);
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "generate=>error: ", e);
        } catch (InvalidKeyException e) {
            Log.e(TAG, "generate=>error: ", e);
        }
        return null;
    }
}
