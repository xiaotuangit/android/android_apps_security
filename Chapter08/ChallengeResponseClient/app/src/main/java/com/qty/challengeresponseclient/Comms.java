package com.qty.challengeresponseclient;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class Comms {

    private static final String TAG = "Comms";

    private static final String url = "http://192.168.3.117:8080/ChallengeResponse/login";
    private Context ctx;
    private DefaultHttpClient client;

    public Comms(Activity act) {
        ctx = act.getApplicationContext();
        client = new DefaultHttpClient();
    }

    public String sendResponse(String hash) {
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("challenge", hash));
        String paramString = URLEncodedUtils.format(params, "utf-8");
        String cUrl = url + "?" + paramString;
        return doGetAsString(cUrl);
    }

    public boolean authorized(String response) {
        String reply = "";
        if (response != null) {
            InputStream is = new ByteArrayInputStream(response.getBytes());
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = null;
            Document doc = null;
            try {
                db = dbFactory.newDocumentBuilder();
                doc = db.parse(is);
                NodeList nl = doc.getElementsByTagName("Response");
                reply = nl.item(0).getTextContent();
                is.close();
            } catch (ParserConfigurationException e) {
                Log.e(TAG, "authorized=>error: ", e);
            } catch (IOException e) {
                Log.e(TAG, "authorized=>error: ", e);
            } catch (SAXException e) {
                Log.e(TAG, "authorized=>error: ", e);
            }
        } else {
            Log.e(TAG, "authorized=>respnse is null.");
        }
        return reply.matches("Authorized");
    }

    public String getChallenge() {
        String challenge = "";
        InputStream challengeText = doGetAsInputStream(url);
        if (challengeText != null) {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = null;
            Document doc = null;
            try {
                db = dbFactory.newDocumentBuilder();
                doc = db.parse(challengeText);
                NodeList nl = doc.getElementsByTagName("Challenge");
                challenge = nl.item(0).getTextContent();
                challengeText.close();
            } catch (ParserConfigurationException e) {
                Log.e(TAG, "getChallenge=>error: ", e);
            } catch (IOException e) {
                Log.e(TAG, "getChallenge=>error: ", e);
            } catch (SAXException e) {
                Log.e(TAG, "getChallenge=>error: ", e);
            }
        } else {
            Log.e(TAG, "getChallenge=>challenge input stream is null.");
        }
        return challenge;
    }

    public String doGetAsString(String url) {
        HttpGet request = new HttpGet(url);
        String result = "";
        try {
            HttpResponse response = client.execute(request);
            int code = response.getStatusLine().getStatusCode();
            if (code == 200) {
                result = EntityUtils.toString(response.getEntity());
            } else {
                Toast toast = Toast.makeText(ctx, "Status Code " + code, Toast.LENGTH_SHORT);
                toast.show();
            }
        } catch (ClientProtocolException e) {
            Log.e(TAG, "doGetAsString=>error: ", e);
        } catch (IOException e) {
            Log.e(TAG, "doGetAsString =>error: ", e);
        }
        return result;
    }

    public InputStream doGetAsInputStream(String url) {
        HttpGet request = new HttpGet(url);
        InputStream result = null;
        try {
            HttpResponse response = client.execute(request);
            int code = response.getStatusLine().getStatusCode();
            if (code == 200) {
                result = response.getEntity().getContent();
            } else {
                Toast toast = Toast.makeText(ctx, "Status Code " + code, Toast.LENGTH_SHORT);
                toast.show();
            }
        } catch (ClientProtocolException e) {
            Log.e(TAG, "doGetAsInputStream=>error: ", e);
        } catch (IOException e) {
            Log.e(TAG, "doGetAsInputStream=>error: ", e);
        }
        return result;
    }
}
