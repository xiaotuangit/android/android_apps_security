ChallengeResponseClient

应用功能说明
    基于挑战/响应的示例代码

应用技术要点说明
    （1）使用Mac对数据进行加密，主要代码如下：
        Mac mac = null;
        try {
            mac = Mac.getInstance(algo);
            String keyText = pass.getText().toString();
            SecretKey key = new SecretKeySpec(keyText.getBytes(), algo);
            mac.init(key);
            byte[] tmpHash = mac.doFinal(server);
            TextView user = (TextView) activity.findViewById(R.id.editText1);
            String username = user.getText().toString();
            String concat = username + " " + Hex.toHex(tmpHash);
            return Base64.encodeToString(concat.getBytes(), Base64.URL_SAFE);
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "generate=>error: ", e);
        } catch (InvalidKeyException e) {
            Log.e(TAG, "generate=>error: ", e);
        }