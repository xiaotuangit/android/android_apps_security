package com.qty.oauthpicasa;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class OAuthPicasaActivity extends ListActivity {

    private static final String TAG = "OAuthPicasa";

    private OAuth oAuth;

    /** Called when the activity is first created. */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        oAuth = new OAuth(this);
        String[] names = new String[]{};    // Add bridge code here to parse XML
                                            // from DataFectcher and populate
                                            // your List
        setListAdapter(new ArrayAdapter<String>(this, R.layout.list_item, names));

        getListView().setTextFilterEnabled(true);
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getApplicationContext(), ((TextView)view).getText(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Token t = oAuth.getToken();

        if (!t.isValidForReq()) {
            Intent intent = new Intent(this, AuthActivity.class);
            startActivity(intent);
        }

        if (t.isExpired()) {
            new GetTokenTask().execute(new Void[]{});
        } else {
            new DataFetcherTask().execute(new Void[]{});
        }
    }

    private class GetTokenTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            return oAuth.getRequestToken();
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            Log.d(TAG, "onPostExecute=>boolean: " + aBoolean);
            if (aBoolean) {
                new DataFetcherTask().execute(new Void[]{});
            }
        }
    }

    private class DataFetcherTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... voids) {
            DataFetcher df = new DataFetcher(oAuth.getToken());
            return df.fetchAlbums("sheranapress");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d(TAG, "onPostExecute=>s: " + s);
            // Add bridge code here to parse XML
            // from DataFectcher and populate
            // your List
            String[] names = new String[]{};
            setListAdapter(new ArrayAdapter<String>(OAuthPicasaActivity.this, R.layout.list_item, names));
        }
    }
}
