package com.qty.oauthpicasa;

import android.app.Activity;
import android.content.Context;
import android.util.Base64;
import android.util.Log;
import android.webkit.WebView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.io.StreamCorruptedException;
import java.io.UnsupportedEncodingException;
import java.net.HttpRetryException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class OAuth {

    private static final String TAG = "OAuth";

    private BasicNameValuePair clientId = new BasicNameValuePair("client_id", "200744748489.apps.googleusercontent.com");
    private BasicNameValuePair clientSecret = new BasicNameValuePair("client_secret", "edxCTl_L8_SFl1rz2klZ4DbB");
    private BasicNameValuePair redirectURI = new BasicNameValuePair("redirect_uri", "urn:ietf:wg:oauth:2.0:oob");
    private String scope = "scope=https://picasaweb.google.com/data/";
    private String oAuth = "https://accounts.google.com/o/oauth2/auth?";
    private String httpReqPost = "https://accounts.google.com/o/oauth2/token";
    private final String FILENAME = ".oauth_settings";
    private URI uri;
    private WebView wv;
    private Context ctx;
    private Activity activity;
    private boolean authenticated;
    private Token token;

    public OAuth(Activity act) {
        ctx = act.getApplicationContext();
        activity = act;
        token = readToken();
    }

    public Token readToken() {
        Token token = null;
        FileInputStream fis;
        try {
            fis = ctx.openFileInput(FILENAME);
            ObjectInputStream in = new ObjectInputStream(new BufferedInputStream(fis));
            token = (Token) in.readObject();
            if (token == null) {
                token = new Token();
                writeToken(token);
            }
            in.close();
            fis.close();
        } catch (FileNotFoundException e) {
            Log.e(TAG, "readToken=>error: ", e);
            writeToken(new Token());
        } catch (OptionalDataException e) {
            Log.e(TAG, "readToken=>error: ", e);
        } catch (StreamCorruptedException e) {
            Log.e(TAG, "readToken=>error: ", e);
        } catch (IOException e) {
            Log.e(TAG, "readToken=>error: ", e);
        } catch (ClassNotFoundException e) {
            Log.e(TAG, "readToken=>error: ", e);
        }
        return token;
    }

    public void writeToken(Token token) {
        try {
            File f = new File(FILENAME);
            if (f.exists()) {
                f.delete();
            }
            FileOutputStream fos = ctx.openFileOutput(FILENAME, Context.MODE_PRIVATE);

            ObjectOutputStream out = new ObjectOutputStream(new BufferedOutputStream(fos));
            out.writeObject(token);
            out.close();
            fos.close();
        } catch (FileNotFoundException e) {
            Log.e(TAG, "Error creating setting file");
        } catch (IOException e) {
            Log.e(TAG, "writeToken=>error: ", e);
        }
    }

    public boolean getRequestToken() {
        boolean result = false;
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost post = new HttpPost(httpReqPost);
        List<NameValuePair> nvPairs = new ArrayList<>();
        nvPairs.add(clientId);
        nvPairs.add(clientSecret);
        nvPairs.add(new BasicNameValuePair("code", token.getAuthCode()));
        nvPairs.add(redirectURI);
        nvPairs.add(new BasicNameValuePair("grant_type", "authorization_code"));
        try {
            post.setEntity(new UrlEncodedFormEntity(nvPairs));
            HttpResponse response = httpClient.execute(post);
            HttpEntity httpEntity = response.getEntity();
            String line = EntityUtils.toString(httpEntity);
            JSONObject jObj = new JSONObject(line);
            token.buildToken(jObj);
            writeToken(token);
            result = true;
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "getRequestToken=>error: ", e);
        } catch (IOException e) {
            Log.e(TAG, "getRequestToken=>error: ", e);
        } catch (JSONException e) {
            Log.e(TAG, "getRequestToken=>error: ", e);
        }
        return result;
    }

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }

}
