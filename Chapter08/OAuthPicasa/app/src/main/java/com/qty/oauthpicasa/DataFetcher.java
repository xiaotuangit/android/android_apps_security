package com.qty.oauthpicasa;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class DataFetcher {

    private static final String TAG = "DataFetcher";

    private HttpClient httpClient;
    private Token token;

    public DataFetcher(Token t) {
        token = t;
        httpClient = new DefaultHttpClient();
    }

    public String fetchAlbums(String userId) {
        String url = "https://picasaweb.google.com/data/feed/api/user/" + userId;
        try {
            HttpResponse resp = httpClient.execute(buildGet(token.getAccessToken(), url));
            if (resp.getStatusLine().getStatusCode() == 200) {
                HttpEntity httpEntity = resp.getEntity();
                return EntityUtils.toString(httpEntity);
            }
        } catch (ClientProtocolException e) {
            Log.e(TAG, "fetchAlbums=>error: ", e);
        } catch (IOException e) {
            Log.e(TAG, "fetchAlbums=>error: ", e);
        }
        return null;
    }

    public HttpGet buildGet(String accessToken, String url) {
        HttpGet get = new HttpGet(url);
        get.addHeader("Autherization", "Bearer " + accessToken);
        return get;
    }
}
