OAuthPicasa

应用功能说明
    演示OAuth开发授权的使用方法

应用技术要点说明
    （1）对象读写流的使用（读写的对象必须继承Serializable，实现序列化对象），主要代码如下：
        1. 写入对象到文件
            public void writeToken(Token token) {
                try {
                    File f = new File(FILENAME);
                    if (f.exists()) {
                        f.delete();
                    }
                    FileOutputStream fos = ctx.openFileOutput(FILENAME, Context.MODE_PRIVATE);

                    ObjectOutputStream out = new ObjectOutputStream(new BufferedOutputStream(fos));
                    out.writeObject(token);
                    out.close();
                    fos.close();
                } catch (FileNotFoundException e) {
                    Log.e(TAG, "Error creating setting file");
                } catch (IOException e) {
                    Log.e(TAG, "writeToken=>error: ", e);
                }
            }

        2. 从文件中读取对象
            public Token readToken() {
                Token token = null;
                FileInputStream fis;
                try {
                    fis = ctx.openFileInput(FILENAME);
                    ObjectInputStream in = new ObjectInputStream(new BufferedInputStream(fis));
                    token = (Token) in.readObject();
                    if (token == null) {
                        token = new Token();
                        writeToken(token);
                    }
                    in.close();
                    fis.close();
                } catch (FileNotFoundException e) {
                    Log.e(TAG, "readToken=>error: ", e);
                    writeToken(new Token());
                } catch (OptionalDataException e) {
                    Log.e(TAG, "readToken=>error: ", e);
                } catch (StreamCorruptedException e) {
                    Log.e(TAG, "readToken=>error: ", e);
                } catch (IOException e) {
                    Log.e(TAG, "readToken=>error: ", e);
                } catch (ClassNotFoundException e) {
                    Log.e(TAG, "readToken=>error: ", e);
                }
                return token;
            }

    （2）监听网页标题。
        创建一个继承WebChromeClient的类，并重写里面的onReceivedTitle方法：
            public class ClientHandler extends WebChromeClient {
                @Override
                public void onReceivedTitle(WebView view, String title) {
                }
            }
    然后，设置该类为WebView的WebChromeClient:
            wv = (WebView) findViewById(R.id.webview);
            wv.setWebChromeClient(new ClientHandler(this));

    （3）监听网页关闭事件。
        创建一个继承WebViewClient的类，并重写里面的onPageFinished方法：
            public class MWebClient extends WebViewClient {
                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                }
            }
    然后，设置该类为WebView的WebViewClient：
            wv = (WebView) findViewById(R.id.webview);
            wv.setWebViewClient(new MWebClient());

    （4）设置WebView启用JavaScript，主要代码如下：
        wv.getSettings().setJavaScriptEnabled(true);