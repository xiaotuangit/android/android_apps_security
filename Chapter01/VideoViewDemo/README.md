#VideoViewDemo

该示例程序演示了使用VideoView播放视频

注意：
        （1）必须为工程添加读取存储权限。
        （2）视频格式必须是平台支持的格式，比如mp4。
        （3）视频文件路径必须书写正确，比如：
                private String path = "/storage/emulated/0/test.mp4";
