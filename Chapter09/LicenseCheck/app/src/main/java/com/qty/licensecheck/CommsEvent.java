package com.qty.licensecheck;

public interface CommsEvent {

    void onTextReceived(String text);

}
