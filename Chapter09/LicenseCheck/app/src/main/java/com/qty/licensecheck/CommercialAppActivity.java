package com.qty.licensecheck;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CommercialAppActivity extends Activity implements CommsEvent {

    private static final String TAG = "CommercialApp";

    private Activity activity;
    private TextView view;
    private CommsEvent event;

    /** Called when the actvity is first created. */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commercial_app);

        activity = this;
        event = this;

        view = (TextView)findViewById(R.id.editText1);

        // verify license on first run

        // Click Button
        final Button button = (Button) findViewById(R.id.button1);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.setText("Fetching fact...");
                CommsNotifier c = new CommsNotifier(event);
                c.start();
            }
        });
    }

    @Override
    public void onTextReceived(final String text) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                view.setText(text);
            }
        });
    }
}
