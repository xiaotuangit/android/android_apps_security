package com.qty.chucknorrisfacts;

public class CommsNotifier extends Thread {

    private CommsEvent event;

    public CommsNotifier(CommsEvent evt) {
        event = evt;
    }

    @Override
    public void run() {
        Comms c = new Comms();
        event.onTextReceived(c.get());
    }
}
