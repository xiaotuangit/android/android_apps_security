package com.qty.chucknorrisfacts;

import android.app.Activity;
import android.content.Context;
import android.location.GpsStatus;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.vending.licensing.AESObfuscator;
import com.google.android.vending.licensing.LicenseChecker;
import com.google.android.vending.licensing.LicenseCheckerCallback;
import com.google.android.vending.licensing.ServerManagedPolicy;
import com.google.android.vending.licensing.util.Base64;
import com.google.android.vending.licensing.util.Base64DecoderException;

import java.util.UUID;

public class ChuckNorrisFactsActivity extends Activity implements CommsEvent {

    private static final String TAG = "ChuckNorrisFacts";

    // Add your Base64 Public
    private static final String PUB_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlXLnY54Y62odQOcizrYgGuTz1f0OYCnSqv5FUX475uCkLZBCr+9OMZkiW/koxw/ujIpNNyu+AgcP7fTla64ylGKQ2o7IUmzxzJDAitN+/uxdbVqXu6LhvxHjggSDI+g8QYs4LO2lLqyeFddfpS/EkOoFD7aQ0GRZzgyY6eW4dwZ3BML9jXKtj6T37BlgPDv5SjK8chECMOc7IpIh/K6TYX28X9kyyiUK7UWtuaUl99iD9Qyisfwp+8xZlQDNPclWbwZz+SojsNjs9Yh3ISUOFcF/BqxZbiMWhRFj9lLwo+xiTXaNErMspjc4O/vNOuHV9mwAm+ire+c7Fpv6vuSpIwIDAQAB";
    // Generate your own 20 random bytes, and put them here.
    private static final byte[] SALT = new byte[] {
            -46, 65, 30, -128, -103, -57, 74, -64, 51, 88, -95, -45, 77, -117, -36, -113, -11, 32, -64,
            89
    };

    private Button button;
    private TextView view;
    private Activity activity;
    private CommsEvent event;
    private LicCallBack lcb;
    private LicenseChecker mChecker;

    /** Called when the activity is first created. */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_chuck_norris_facts);

        try {
            Log.d(TAG, "onCreate=>action: " + new String(
                    Base64.decode("Y29tLmFuZHJvaWQudmVuZGluZy5saWNlbnNpbmcuSUxpY2Vuc2luZ1NlcnZpY2U=")));
        } catch (Base64DecoderException e) {
            Log.e(TAG, "onCreate=>error: ", e);
        }

        event = this;
        activity = this;
        view = (TextView) findViewById(R.id.editText1);

        // Click Button
        button = (Button) findViewById(R.id.button1);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Do License Check before allowing click

                // Generate a Unique ID
                String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                Context ctx = activity.getApplicationContext();

                // Create an Obfuscator and a Policy
                //AESObfuscator obf = new AESObfuscator(SALT, getPackageName(), deviceId);
                //ServerManagedPolicy policy = new ServerManagedPolicy(ctx, obf);

                // Create the LicenseChecker
                //mChecker = new LicenseChecker(ctx, policy, PUB_KEY);
                mChecker = new LicenseChecker(
                        getApplicationContext(),
                        new ServerManagedPolicy(getApplicationContext(), new AESObfuscator(SALT, getPackageName(), deviceId)),
                        PUB_KEY  // Your public licensing key.
                );

                // Do the license check
                lcb = new LicCallBack();
                mChecker.checkAccess(lcb);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mChecker.onDestroy();
    }

    @Override
    public void onTextReceived(final String text) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setProgressBarIndeterminateVisibility(false);
                view.setText(text);
                button.setEnabled(true);
            }
        });
    }

    public class LicCallBack implements LicenseCheckerCallback {

        @Override
        public void allow(int reason) {
            if (isFinishing()) {
                return;
            }
            Toast toast = Toast.makeText(getApplicationContext(), "Licensed!", Toast.LENGTH_SHORT);
            toast.show();
            button.setEnabled(false);
            setProgressBarIndeterminateVisibility(true);
            view.setText("Fetching fact...");
            CommsNotifier c = new CommsNotifier(event);
            c.start();
        }

        @Override
        public void dontAllow(int reason) {
            if (isFinishing()) {
                return;
            }
            Toast toast = Toast.makeText(getApplicationContext(), "Unlicensed!", Toast.LENGTH_LONG);
            toast.show();
        }

        @Override
        public void applicationError(int errorCode) {
            Log.e(TAG, "applicationError=>error code: " + errorCode);
        }
    }
}
