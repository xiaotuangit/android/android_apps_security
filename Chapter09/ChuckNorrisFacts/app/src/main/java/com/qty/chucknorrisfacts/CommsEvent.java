package com.qty.chucknorrisfacts;

public interface CommsEvent {

    void onTextReceived(String text);

}
