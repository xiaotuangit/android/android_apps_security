ChuckNorrisFacts

应用程序功能说明
    使用Android License Verification Library对应用程序进行许可验证。

应用程序技术要点说明
    （1） 使用Android License Verification Library验证应用程序。
        1. 下载Android License Verification Library
            可以通过Android SDK Manager工具进行下载，在SDK Tool项中找到Google Play Licensing Library，
        点击下载即可。
        2. 将Android License Verification Library添加到工程中。
            在AndroidStudio中点击"File -> New -> import Module..."菜单，在弹出的对话框中选择Android
        License Verification Library的路径（在AndroidSdKPath/extras/google/market_licensing/Library中），
        最后点击“确定”按钮即可。
        3. 添加Android License Verification Library权限到AndroidManifest.xml
            <!-- Required permission to check licensing. -->
            <uses-permission android:name="com.android.vending.CHECK_LICENSE" />
        4.验证应用程序的主要代码如下：
            // Generate a Unique ID
            String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
            Context ctx = activity.getApplicationContext();

            // Create an Obfuscator and a Policy
            //AESObfuscator obf = new AESObfuscator(SALT, getPackageName(), deviceId);
            //ServerManagedPolicy policy = new ServerManagedPolicy(ctx, obf);

            // Create the LicenseChecker
            //mChecker = new LicenseChecker(ctx, policy, PUB_KEY);
            LicenseChecker checker = new LicenseChecker(
                    getApplicationContext(),
                    new ServerManagedPolicy(getApplicationContext(), new AESObfuscator(SALT, getPackageName(), deviceId)),
                    PUB_KEY  // Your public licensing key.
            );

            // Do the license check
            LicCallBack lcb = new LicCallBack();
            checker.checkAccess(lcb);
    （2）注意: 这个程序在运行中会报错，暂时没有找到问题的原因，报错说没有明确指定Intent Action。可能是没有该Action的服务吧。
同时，Android LVL也不能对Debug版本的应用程序进行验证。
    （3）最好是将Android LVL库源代码拷贝到其他地方来创建库项目。
    （4）其他信息可参考官方文档： https://developer.android.com/google/play/licensing/